import React, { Component } from "react";
import request from "superagent";
import SimpleLineChart from "./SimpleLineChart";

export default class CarForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFirstnameChange = this.handleFirstnameChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.sendDocument = this.sendDocument.bind(this);
        this.state = {
            isLoading: false,
        }
        this.state = { name: '', price: null };
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            name="carName"
                            onChange={this.handleFirstnameChange}
                            value={this.state.carName}
                            placeholder="Votre nom de voiture"
                        />
                        <SimpleLineChart />
                         <input
                            type="number"
                            step="0.01"
                            name="carPrice"
                            onChange={this.handlePriceChange}
                            value={this.state.carPrice}
                            placeholder="Votre nom de voiture"
                        />
                        <input type="submit" value={!this.state.isLoading ? 'Ajouter' : 'Enregistrement...'} disabled={this.state.isLoading} />
                    </div>
                </form>
                <form onSubmit={this.sendDocument}>
                    <div className="form-group">
                        <input
                            type="file"
                            name="carFile"
                            ref={el => this.fileInput = el} 
                            placeholder="fichier à uploader"
                        />
                        <input type="submit" value={!this.state.isLoading ? 'Ajouter' : 'Enregistrement...'} disabled={this.state.isLoading} />
                    </div>
                </form>
            </div>
        );
    }
    sendDocument(event) {
        event.preventDefault();
        this.setState({ isLoading: true });
        request
            .post(`http://localhost:8080/cars/file`)
            .attach('file', this.fileInput.files[0])
            .then(
                (response) => {
                    this.setState({ isLoading: false });
                    console.log(response);
                }
            );
    }
    handleFirstnameChange(event) {
        this.setState({
            name: event.target.value
        });
    }
    handlePriceChange(event) {
        this.setState({
            price: event.target.value
        })
    }
    handleSubmit(event) {
        event.preventDefault();
        this.setState({ isLoading: true });
        try {
            request
                .post(`http://localhost:8080/cars`)
                .send( {"name": this.state.name, "price": this.state.price})
                .set('Accept', 'application/json')
                .then(
                    (response) => {
                        this.setState({ isLoading: false });
                    }
                );
        } catch (error) {
            console.error(error)
        } finally {
            this.setState({ isLoading: false });
            this.setState({ carName: '' });
        }
    }
}