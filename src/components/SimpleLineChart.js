import React from 'react';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import LineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';

function SimpleLineChart(data) {
  return (
    // 99% per https://github.com/recharts/recharts/issues/172
    <ResponsiveContainer width="99%" height={320}>
      <LineChart data={data.data}>
        <XAxis dataKey="date" />
        <YAxis />
        <CartesianGrid vertical={false} strokeDasharray="3 3" />
        <Tooltip />
        <Legend />
        <Line name="Production type A" type="monotone" dataKey="typeProductionMap.A" stroke="#82ca9d" activeDot={{ r: 4 }} />
        <Line name="Production type B" type="monotone" dataKey="typeProductionMap.B" stroke="#8884d8" activeDot={{ r: 4 }} />
        <Line name="Production type C" type="monotone" dataKey="typeProductionMap.C" stroke="#52b202" activeDot={{ r: 4 }} />
        <Line name="Total production" type="monotone" dataKey="totalProduction" stroke="#b2102f" activeDot={{ r: 4 }} />
      </LineChart>
    </ResponsiveContainer>
  );
}

export default SimpleLineChart;