import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { mainListItems } from './listItems';
import SimpleLineChart from './SimpleLineChart';
import SimpleTable from './SimpleTable';
import request from 'superagent';
import { Input, InputLabel, FormControl, Button } from '@material-ui/core';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 130,
  },
  uploadFileField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 285
  },
  tableContainer: {
    height: 320,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
});

class Dashboard extends React.Component {

  state = {
    open: true,
    production: null,
    products: null,
    beginningDate: null,
    endDate: null
  };

  componentDidMount() {
    var config = require('Config')
    request
      .get(`${config.serverUrl}/jeyni-app/api/productions`)
      .then((response) => {
        console.debug(response.body)
        this.setState({ production: response.body });
      });
    request
      .get(`${config.serverUrl}/jeyni-app/api/cars/types`)
      .then((response) => {
        let products = response.body;
        let i = 0;
        products.forEach(element => {
          element.id = i;
          i++;
        });
        console.debug(response.body)
        this.setState({ products: response.body });
      });
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  sendDate = () => {
    let config = require('Config')
    let beginningDate = reverseDate(this.state.beginningDate)
    let enDate = reverseDate(this.state.endDate)
    request
      .get(`${config.serverUrl}/jeyni-app/api/productions/dates`)
      .set('Accept', 'application/json')
      .query({ beginningDate: beginningDate })
      .query({ endDate: enDate })
      .then((response) => {
        console.debug(response.body)
        this.setState({ production: response.body });
      });
    request
      .get(`${config.serverUrl}/jeyni-app/api/cars/types/dates`)
      .set('Accept', 'application/json')
      .query({ beginningDate: beginningDate })
      .query({ endDate: enDate })
      .then((response) => {
        let products = response.body;
        let i = 0;
        products.forEach(element => {
          element.id = i;
          i++;
        });
        console.debug(response.body)
        this.setState({ products: response.body });
      });
  }

  sendFile = () => {
    let config = require('Config')
    console.log(this.fileInput.files[0])
    request
      .post(`${config.serverUrl}/jeyni-app/api/productions/file`)
      .attach('file', this.fileInput.files[0])
      .then(
        (response) => {
          console.log(response);
          request
            .get(`${config.serverUrl}/jeyni-app/api/productions`)
            .then((response) => {
              console.debug(response.body)
              this.setState({ production: response.body });
            });
          request
            .get(`${config.serverUrl}/jeyni-app/api/cars/types`)
            .then((response) => {
              let products = response.body;
              let i = 0;
              products.forEach(element => {
                element.id = i;
                i++;
              });
              console.debug(response.body)
              this.setState({ products: response.body });
            });
        }
      );

  }


  handleBeginningDateChange = (event) => {
    this.setState({ beginningDate: event.target.value });
  }

  handleEndDateChange = (event) => {
    this.setState({ endDate: event.target.value });
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <div className={classes.root}>
          <AppBar
            position="absolute"
            className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
          >
            <Toolbar disableGutters={!this.state.open} className={classes.toolbar}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(
                  classes.menuButton,
                  this.state.open && classes.menuButtonHidden,
                )}
              >
                <MenuIcon />
              </IconButton>
              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                className={classes.title}
              >
                Dashboard
              </Typography>
              <IconButton color="inherit">
                <Badge badgeContent={4} color="secondary">
                  <NotificationsIcon />
                </Badge>
              </IconButton>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            classes={{
              paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
            }}
            open={this.state.open}
          >
            <div className={classes.toolbarIcon}>
              <IconButton onClick={this.handleDrawerClose}>
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />
            <List>{mainListItems}</List>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Typography variant="h4" gutterBottom component="h2">
              Production
            </Typography>
            <Typography component="div" className={classes.chartContainer}>
              <SimpleLineChart data={this.state.production} />
            </Typography>
            <div>
              <FormControl>
                <InputLabel htmlFor="begginning-date-input" shrink={true}>
                  Begginning date
              </InputLabel>
                <Input
                  id="begginning-date-input"
                  type="date"
                  className={classes.textField}
                  onChange={this.handleBeginningDateChange}
                  margin="dense" />
              </FormControl>
              <FormControl>
                <InputLabel htmlFor="end-date-input" shrink={true}>
                  End date
                </InputLabel>
                <Input
                  id="end-date-input"
                  type="date"
                  className={classes.textField}
                  onChange={this.handleEndDateChange}
                  margin="dense" />
              </FormControl>
              <Button variant="contained" color="primary" className={classes.button} onClick={this.sendDate}>
                Send
              </Button>
            </div>
            <Typography variant="h4" gutterBottom component="h2">
              Products
            </Typography>
            <div className={classes.tableContainer}>
              <SimpleTable data={this.state.products ? this.state.products : dataTable} />
            </div>
            <div>
              <Input
                type="file"
                id="file-input"
                className={classes.uploadFileField}
                inputRef={el => this.fileInput = el} >
              </Input>
              <Button variant="contained" color="primary" className={classes.button} onClick={this.sendFile}>
                upload
              </Button>
            </div>
          </main>
        </div>
      </React.Fragment>
    );
  }
}

function reverseDate(date) {
  var res = date.split("-");
  var day = res[2]
  var month = res[1]
  var year = res[0]
  var dateReversed = day + "/" + month + "/" + year
  return dateReversed
}

let id = 0;
function createData(name, calories, fat, carbs, protein) {
  id += 1;
  return { id, name, calories, fat, carbs, protein };
}

const dataTable = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Dashboard);