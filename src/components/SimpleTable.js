import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

function SimpleTable(props) {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Car (type)</TableCell>
            <TableCell numeric>Cost (€)</TableCell>
            <TableCell numeric>Sell price (€)</TableCell>
            <TableCell numeric>Total cost price (€)</TableCell>
            <TableCell numeric>Total sell price (€)</TableCell>
            <TableCell numeric>Total production</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {!props.data ? null : props.data.map(n => {
            return (
              <TableRow key={n.id}>
                <TableCell component="th" scope="row">
                  {n.modelType}
                </TableCell>
                <TableCell numeric>{n.singleCostPrice}</TableCell>
                <TableCell numeric>{n.singleSellPrice}</TableCell>
                <TableCell numeric>{n.totalCostPrice}</TableCell>
                <TableCell numeric>{n.totalSellPrice}</TableCell>
                <TableCell numeric>{n.production}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);