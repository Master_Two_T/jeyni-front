import React, { Component } from "react";
import CarForm from "./CarForm";
import Dashboard from "./Dashboard";

import '../styles/App.css';

class App extends Component {
    render() {
        return (
            <div>
                <Dashboard />
            </div>
        );
    }
}

export default App;